
Sonofman
===========
# aka BibleMultiTheSonOfMan  


Bible multi languages, free, offline, no advertising, in English, French, Italian, Spanish, Portuguese for **Terminal**.

Bibles included: King James Version, Louis Segond, Ostervald, Diodati, Reina Valera, Almeida, Schlachter, Elberfelder, Romanian Bible (Romanian Cornilescu 1928), Polish Bible (Biblia Warszawska 1975), Russian Bible (Russian Synodal Translation 1876), Turkish Bible (New Turkish Bible 2001), Swahili Bible (Swahili Union Version 1997), Arabic Bible (Smith & Van Dyke), Hindi Bible, Bengali Bible (Bengali C.L. Bible 2016), Chinese Bible (Chinese Union Version Simplified), Japanese Bible (New Japanese Bible 1973).

Easy to use with quick searches, parables, articles, cross references, favorites, history.

As there is not a lot of Bible applications in Terminal, my app can be useful.
The app exists in several packages: Flatpak, Snap, PyPI and can be installed on Linux, RaspberryPI, all ARM, Mac, Windows (Cygwin), Android (Termux).

Works with Python and Ncurses.
The app is very low in memory and CPU usage.
Check my website if you need more graphical interfaces. The app has two brothers :)

Please share the info with your friends. Time is short. Tribulations are at the door.

** All The Glory To God.

To install the app on ARM (all functions work), the best solution remains to use the PyPI package. Snap and Flatpak would be too heavy for the processor: https://pypi.org/project/sonofman/

For the story: the app was created after Bible Multi The Light (Android) and before Bible Multi The Life (Android, iPhone, iPad, Big Sur, Mac, Linux).
You can find them on their stores or via my website: https://www.biblemulti.org



+----------------------------------------+
|                                        |
| Works on Linux, Mac, RaspberryPI, ARM, |
|   Android (Termux), Windows (Cygwin)   |
|                                        |
|            With Python 3.9+            |
|                                        |
+----------------------------------------+

Installation with Flatpak
-------------------------
.. code-block:: console

    $ flatpak install flathub org.hlwd.sonofman



Run with Flatpak
----------------
.. code-block:: console

    $ flatpak run org.hlwd.sonofman



Installation with Snap
----------------------
.. code-block:: console

    $ snap install bible-multi-the-son-of-man



Run with Snap
-------------
.. code-block:: console

    $ bible-multi-the-son-of-man



Installation with pip
---------------------
.. code-block:: python 

    pip install sonofman



Run with pip
------------
.. code-block:: console

    $ sonofman
    
or

.. code-block:: console

    $ ./som



Tips
----

* If you have problem in your Terminal when quitting the application, type "reset" to restore the terminal or CTRL-D.

* If characters are missing, please install the utf8 characters with "sudo dpkg-reconfigure locales": select en_US, es_ES, fr_FR, it_IT, pt_PT...



Help
----

* Don't hesitate to talk about the application on social media...

* Don't hesitate to test the application and inform me about suggestions, bugs...



Notes
-----

* If you want to use the app on Windows, please install Cygwin.

* Due to wide chars some Hindi characters could be replaced.

* Customization of colors have been temporary disabled for Hindi and Bengali because it's not always displayed correctly.



Screenshots
-----------

.. image:: https://gitlab.com/hotlittlewhitedog/BibleMultiTheSonOfMan/raw/master/screenshots/som12.png
    :alt: Screenshot

.. image:: https://gitlab.com/hotlittlewhitedog/BibleMultiTheSonOfMan/raw/master/screenshots/som03.png
    :alt: Screenshot

.. image:: https://gitlab.com/hotlittlewhitedog/BibleMultiTheSonOfMan/raw/master/screenshots/som06.png
    :alt: Screenshot

.. image:: https://gitlab.com/hotlittlewhitedog/BibleMultiTheSonOfMan/raw/master/screenshots/som07.png
    :alt: Screenshot

.. image:: https://gitlab.com/hotlittlewhitedog/BibleMultiTheSonOfMan/raw/master/screenshots/som08.png
    :alt: Screenshot

.. image:: https://gitlab.com/hotlittlewhitedog/BibleMultiTheSonOfMan/raw/master/screenshots/som09.png
    :alt: Screenshot
    
.. image:: https://gitlab.com/hotlittlewhitedog/BibleMultiTheSonOfMan/raw/master/screenshots/som04.png
    :alt: Screenshot

.. image:: https://gitlab.com/hotlittlewhitedog/BibleMultiTheSonOfMan/raw/master/screenshots/som05.png
    :alt: Screenshot

.. image:: https://gitlab.com/hotlittlewhitedog/BibleMultiTheSonOfMan/raw/master/screenshots/som10.png
    :alt: Screenshot

.. image:: https://gitlab.com/hotlittlewhitedog/BibleMultiTheSonOfMan/raw/master/screenshots/som11.png
    :alt: Screenshot
