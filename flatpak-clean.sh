#!/bin/sh
rm -rf .flatpak-builder
rm -rf dist
rm -rf sonofman.egg-info
rm -rf tests
find . -type d -name "__pycache__" -exec rm -r {} +
find . -type f -name "*.pyc" -delete

if [ -d "build" ]; then
  cd build || exit
  rm -rf *
  cd ..
fi
