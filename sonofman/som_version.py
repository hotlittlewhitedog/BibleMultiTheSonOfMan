#!/usr/bin/env python3


class SomVersion:
    """
    History
    -------------------
    3.3.0 (7)  20240824
    3.2.1 (7)  20240804 - Fixes
    3.2.0 (7)  20240804 - 6 Bibles added
    3.1.0 (6)  20230620 - 5 Bibles added
    3.0.0 (5)  20230418 - RVA89, LND, Update process, Support Mac, Termux, Cygwin
    2.6.0 (4)  20220830 - Website, ABC
    2.5.0 (4)  20220526 - The Rapture Survival Guide
    2.4.1 (4)  20220430 - Search style. Rem: discover that db not under Linux is always fresh installation with PyPI!
    2.3.0 (4)  20220404 - Db updated by code and new install (clipboard)
    2.2.0 (3)  20220327
    2.1.1 (3)  20220315 - Fixes
    2.1.0 (3)  20220311 - Db copied locally for Flatpak, Snap
    2.0.0 (3)  20220220 - Bible of The Life 1.18 (8)
    1.4.0 (2)  20220122
    1.3.0 (2)  20181007
    1.2.0 (2)  20180919
    1.1.0 (2)  20180905
    1.0.0 (1)  20180825
    0.9.5 (1)  20180824
    0.9.4 (1)  20180822
    0.9.3 (1)  20180821
    0.9.2 (1)  20180818
    0.9.1 (1)  20180817
    0.9.0 (1)  20180817
    """

    app_version = '3.3.0'
    app_version_date = '20240824'
    db_version = 7      # db version code

    app_name = 'sonofman'
    author = 'hotlittlewhitedog'
    author_email = 'hotlittlewhitedog@gmail.com'
    website = 'https://www.biblemulti.org'
    telegram = 'https://t.me/biblemultithelight'
    twitter = 'https://www.twitter.com/@_hlwd'
    url = 'https://gitlab.com/hotlittlewhitedog/BibleMultiTheSonOfMan'
    issue = 'https://gitlab.com/hotlittlewhitedog/BibleMultiTheSonOfMan/issues'
    licence = 'GPL3'
